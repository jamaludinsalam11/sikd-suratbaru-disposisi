import React,{useEffect, useState} from 'react'
import { StylesProvider, createGenerateClassName } from '@material-ui/core/styles'
import axios from 'axios'
import DisposisiBaruAll from './components/DisposisiBaruAll'
const generateClassName = createGenerateClassName({
    productionPrefix: 'suratbaru-disposisi',
  });
export default function App(props){
    return(
        <StylesProvider generateClassName={generateClassName}>
            <DisposisiBaruAll/>
         </StylesProvider>
    )
}